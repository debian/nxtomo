.. |nxtomo_icon| image:: img/nxtomo.png


==============
|nxtomo_icon|
==============

the goal of this project is to provide a powerful and user friendly API to create and edit [NXtomo](https://manual.nexusformat.org/classes/applications/NXtomo.html) application


.. toctree::
   :hidden:

   tutorials/index.rst
   api.rst
   user_guide/installation.rst
   development/changelog.rst
